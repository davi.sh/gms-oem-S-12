#!/bin/bash


myfunc()
{
  local file=""

  #Check all Files or Folder at the Parent TOP location
  for tempfile in ./*
  do
        #Remove Path and take only file name
        file=$(basename $tempfile)

        #If it is folder. Go inside and check all files/folder
        if [ -d "$file" ]
        then
          #Go inside the subdirectory
          cd $file

          #Call same function (recuring function) to check all files/subfolders
          myfunc

          #Current folder process is complete. Go back to parent folder
          cd ..
        fi

        #If file is apk, then only check for lib files
        if [[ $file == *.apk ]]
        then
                # Find targetSDK version
                targetsdkversion=$(aapt dump badging $file 2>/dev/null | grep 'targetSdkVersion:' | awk -F "'" '{print $2}')
                echo $file " : " $targetsdkversion

        fi
      done
}

echo -e ""
echo -e "*************************************"
echo -e "TargetSDK Details"
echo -e "************************************"
echo -e ""

myfunc


