$(call inherit-product, vendor/partner_gms/products/gms.mk)

ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),false)
  PRODUCT_PACKAGES += Chrome64 WebViewGoogle64
else
  $(error This build config must be used by 64-bit only targets.)
endif
